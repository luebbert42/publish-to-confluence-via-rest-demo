import os
from atlassian import Confluence 
import yaml
import logging
import sys
import json

def get_config() -> dict:
  return yaml.safe_load(open("config.yml"))

def redirect_rest_logging_to_logfile():
  logger = logging.getLogger("atlassian.rest_client")
  ch = logging.FileHandler('logs/rest_client.log')
  logger.addHandler(ch)

def get_file_content_as_list(filename: str) -> list:
        content = [line.rstrip('\n') for line in open(filename)]
        return content
       
def get_page_properties(file_content: str):
        page_properties = dict()
        page_properties['page_title'] = file_content[0]
        # remove first line as it contains the page title which is not needed in Confluence as part of the body
        file_content.pop(0)
        page_properties['page_body'] = ' '.join(file_content)
        return page_properties

def delete_page(confluence: Confluence, config: dict, page_properties: dict):
   print("deleting ", page_properties["page_title"])
   response = confluence.get_page_id(page_properties["space"],page_properties["page_title"])
   print(response)   

def create_or_update_page(confluence: Confluence, config: dict, page_properties: dict):
  print("creating ", page_properties["page_title"])      
  response = confluence.update_or_create(page_properties["parent_id"], page_properties["page_title"], page_properties["page_body"])
  if ('statusCode' in response) and (response['statusCode'] == 400):
    print("This did not work, response code = 400, which usually means that the content is not parseable by Confluence")
  elif ('id' in response) and (response['status'] == "current"):
    print("Response looks good, check: "+config['confluence_url']+response['_links']['webui'])
  else:
    print("unhandled status, pls review response")
    print(response)
  return response  


"""
   MAIN
"""
root_id = 1343541
space = "GIS"
redirect_rest_logging_to_logfile()
config = get_config()
confluence = Confluence(
    url=config['confluence_url'],
    username=config['confluence_username'],
    password=config['confluence_password'])


parent_file_names = ["__tables.html"]
for root, dirs, files in os.walk("storage/99_db_doku"):
    path = root.split(os.sep)
    # new directory found, set parent_id back to root_id
    parent_id = root_id
    # print((len(path) - 1) * 'd: ---', os.path.basename(root))
    files.sort()
    for file in files:    
        #print(len(path) * 'f: ---', file)
        file_content = get_file_content_as_list(os.path.join(root, file)) 
        page_properties = get_page_properties(file_content)
        page_properties['parent_id'] = parent_id
        page_properties['space'] = space
        if file.endswith("deleted"):
          print("diese Datei muss gelöscht werden", file)
          delete_page(confluence, dict, page_properties)
        else:  
          print("lege datei an",file)
          #response = create_or_update_page(confluence, config, page_properties)
          # switch parent_id if necessary
          #if file in parent_file_names and ('id' in response) and (response['status'] == "current"):
          #      parent_id = response["id"]
          #      print("neue Parent-ID gesetzt", parent_id)



        


