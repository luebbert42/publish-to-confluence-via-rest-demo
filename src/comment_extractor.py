from sqlalchemy import create_engine
from jinja2 import Template
import os
import sys

class CommentExtractor:

  def __init__(self, config: dict):
    for key in config:
      if config[key] is None:
         config[key] = ""
    connectstring = 'postgresql://'+config['db_user']+':'+config['db_password']+'@'+config['db_host']+':'+str(config['db_port'])+"/"+config['db_name']
    self._db = create_engine(connectstring)
    self._db.connect()

  def __get_schemas(self):
      records = self._db.execute("""select schema_name from information_schema.schemata""")
      schemas = []
      for schema in records:
        schemas.append(schema[0])
      return schemas

     
  def __get_table_names(self, schema: str): 
      records = self._db.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema = %(schema)s""", {'schema': schema})
      table_names = []
      for table in records:
        table_names.append(table[0])
      return table_names

  def __get_table_comment(self, schema: str, table_name: str) -> dict:
      result = self._db.execute("""select 
      col_description((table_schema||'.'||table_name)::regclass::oid, ordinal_position)  as column_comment, 
      * from information_schema.columns 
      WHERE table_schema = %(schema)s and table_name = %(table_name)s;""", {'schema': schema, 'table_name': table_name})
      return result


  def __render(self, table_name: str, metadata):
    template = "templates/dbdoc.html.jinja2"
    with open(template) as file_:
      template = Template(file_.read())    
    return template.render(table_name = table_name, metadata = metadata)      
  
  def create_documentation(self): 
        schemas = self.__get_schemas()
        for schema in schemas:
          tables = self.__get_table_names(schema)       
          for table_name in tables:    
            metadata = self.__get_table_comment(schema, table_name)               
            print(self.__render(table_name, metadata))

      






