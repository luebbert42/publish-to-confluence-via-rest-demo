from atlassian import Confluence
import yaml
import logging
import sys
import json
from src.comment_extractor import CommentExtractor

def get_config() -> dict:
  return yaml.safe_load(open("config.yml"))

def redirect_rest_logging_to_logfile():
  logger = logging.getLogger("atlassian.rest_client")
  ch = logging.FileHandler('logs/rest_client.log')
  logger.addHandler(ch)

def test_if_page_exists(confluence: Confluence):
  """ determine from response if page is already created in Confluence or not """
  response = confluence.get_page_by_id(340832262)
  if ('statusCode' in response) and (response['statusCode'] == 404):
    print("Page does not exist")
  elif ('id' in response) and (response['status'] == "current"):
    print("Page already exists")
  else:
    print("unhandled status, pls review response")
    print(response)

def create_or_update_page(confluence: Confluence, config: dict):
  title = "Unique Title 667"
  body = """
  <h1>Hello Confluence</h1>

  <table>
  <tbody>
    <tr>
        <th>Time for a poem</th>
    </tr>
    <tr>
      <td>Mein Vater war ein Wandersmann</td>
    </tr>
    <tr>
      <td>und mir steckt’s auch im Blut.</td>
    </tr>
    <tr>
      <td>D’rum wand’r’ ich froh, so lang ich kann,</td>
    </tr>
    <tr>
      <td>und schwenke meinen Hut.</td>
    </tr>
    </tbody>
    </table>

  <ac:link>
  <ri:page ri:content-title="Dokumentation" />
  <ac:plain-text-link-body>
  <![CDATA[Link to another Confluence Page]]>
  </ac:plain-text-link-body>
  </ac:link> 
"""
  print("hallo!")
  parent_id = 65602
  response = confluence.update_or_create(parent_id, title, body)
  if ('statusCode' in response) and (response['statusCode'] == 400):
    print("This did not work, response code = 400, which usually means that the content is not parseable by Confluence")
  elif ('id' in response) and (response['status'] == "current"):
    print("Response looks good, check: "+config['confluence_url']+response['_links']['webui'])
  else:
    print("unhandled status, pls review response")
    print(response)


redirect_rest_logging_to_logfile()
config = get_config()
confluence = Confluence(
    url=config['confluence_url'],
    username=config['confluence_username'],
    password=config['confluence_password'])
create_or_update_page(confluence, config)

"""
config = get_config()
c = CommentExtractor(config)
c.create_documentation()
"""

