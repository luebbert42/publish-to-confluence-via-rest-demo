# Modify Confluence content via REST interface

## What does this demo do? 

- uses Python module provided by Atlassian to modify Confluence content
- module uses Confluence's REST interface
- content must be provided using Confluence syntax (see link below)
- auth is done via basic auth ... use your regular confluence auth data for testing and a technical user for production

## Installation

- install virtualenv https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
- clone repo ... then

```bash
cd /path/to/local-repository/
# activate environment
source bin/activate

# install dependencies
pip install -r requirements.txt

# create config
cp config.yml.example config.yml

# adjust config.yml with your windows username/pwd (or for prod a tech user)
vim config.yml 

# run script
python publish2confluence.py

```

- the rest client log is in the logs directory


## Read more
- Source code of the used Python module: https://github.com/atlassian-api/atlassian-python-api/tree/master/atlassian
- Documentation of the package: https://atlassian-python-api.readthedocs.io/en/latest/confluence.html#page-actions
- Confluence Syntax (use the html-like syntax!!): https://confluence.atlassian.com/conf612/confluence-storage-format-958777608.html#ConfluenceStorageFormat-Texteffects
- REST doc: https://developer.atlassian.com/cloud/confluence/rest/ (quite fuzzy and the endpoints do not work for us, must be e.g. https://confluence-it.unitymedia.de/rest/api/content)

# Curl example
The authorization line is the base64encoded version of "mustermann.max:TotalGeheimesPasswort!", must be changed to valid settings (Windows-Username/Password)

```bash
curl -X POST \
  https://confluence-it.unitymedia.de/rest/api/content \
  -H 'Authorization: Basic bXVzdGVybWFubi5tYXg6VW5pdHlNZWRpYU1hZ0V4dGVybmVOaWNodEFsbHp1U2VockFiZXJTb25zdElzdEVzR2Fuek5ldHQ0MiE=' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "title": "Mein Vater war ein grosser Wandersmann",
  "type": "page",
  "space": {
    "key": "GIS"
  },
  "status": "current",
  "ancestors": [
    {
      "id": "34083226"
    }
  ],
  "body": {
    "storage": {
      "value": "<h1>Das hat geklappt</h1> ... und mir steckt'\''s auch im Blut. Diese Seite wurde über einen REST Call angelegt. Juhu. :-D ",
      "representation": "storage"
    }
  }
}'
```
## Pure Python example (without any chichi)

```python
import requests

url = "https://confluence-it.unitymedia.de/rest/api/content"

payload = "{\n  \"title\": \"Mein Vater war ein grosser Wandersmann\",\n  \"type\": \"page\",\n  \"space\": {\n    \"key\": \"GIS\"\n  },\n  \"status\": \"current\",\n  \"ancestors\": [\n    {\n      \"id\": \"34083226\"\n    }\n  ],\n  \"body\": {\n    \"storage\": {\n      \"value\": \"<h1>Das hat geklappt</h1> ... und mir steckt's auch im Blut. Diese Seite wurde über einen REST Call angelegt. Juhu. :-D \",\n      \"representation\": \"storage\"\n    }\n  }\n}"
headers = {
    'Content-Type': "application/json",
    'Authorization': "Basic bXVzdGVybWFubi5tYXg6VW5pdHlNZWRpYU1hZ0V4dGVybmVOaWNodEFsbHp1U2VockFiZXJTb25zdElzdEVzR2Fuek5ldHQ0MiE=",
    'cache-control': "no-cache",
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
```
